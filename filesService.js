// requires...
const { log } = require("console");
const fs = require("fs");
const path = require("path");

const FILE_PATH = `./files/`;
const extensions = [".log", ".txt", ".json", ".yaml", ".xml", ".js"];
const updatedText = " 4. Check the result.";

function createFile(req, res, next) {
  const fileExt = path.extname(req.body.filename);

  if (!extensions.includes(fileExt)) {
    next({
      status: 400,
      message:
        "Application supports log, txt, json, yaml, xml, js file extensions",
    });
    return;
  }

  if (req.body.content.length === 0) {
    next({
      status: 400,
      message: "Please specify 'content' parameter",
    });
    return;
  }

  fs.writeFile(FILE_PATH + req.body.filename, req.body.content, (err) => {
    if (err) {
      next({ status: 400, message: err.message });
      return;
    }

    res.status(200).send({ message: "File created successfully" });
  });
}

function getFiles(req, res, next) {
  fs.readdir(FILE_PATH, (err, files) => {
    if (err) {
      next({ status: 400, message: "Client error" });
      return;
    }

    res.status(200).send({
      message: "Success",
      files: files,
    });
  });
}

const getFile = (req, res, next) => {
  let filename = req.url.slice(1);

  fs.readFile(FILE_PATH + filename, "utf-8", (err, data) => {
    if (err) {
      next({
        status: 400,
        message: `No file with '${filename}' filename found`,
      });
      return;
    }

    res.status(200).send({
      message: "Success",
      filename: path.basename(FILE_PATH + filename),
      content: data,
      extension: path.extname(filename).slice(1),
      uploadedDate: fs.statSync(FILE_PATH + filename).birthtime,
    });
  });
};

// Other functions - editFile, deleteFile

const editFile = (req, res, next) => {
  let filename = req.url.slice(1);

  if (!fs.existsSync(FILE_PATH + filename)) {
    next({
      status: 400,
      message: `No file with '${filename}' filename found`,
    });
    return;
  }

  fs.appendFile(FILE_PATH + filename, updatedText, (err) => {
    if (err) {
      next({
        status: 400,
        message: `Something went wrong`,
      });
      return;
    }

    res.status(200).send({ message: "All changes are saved successfully" });
  });
};

const deleteFile = (req, res, next) => {
  let filename = req.url.slice(1);

  if (!fs.existsSync(FILE_PATH + filename)) {
    next({
      status: 400,
      message: `No file with '${filename}' filename found`,
    });
  }

  fs.unlink(FILE_PATH + filename, (err) => {
    if (err) {
      next({
        status: 400,
        message: `Something went wrong`,
      });
      return;
    }

    res.status(200).send({ message: "File deleted successfully" });
  });
};

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile,
};
